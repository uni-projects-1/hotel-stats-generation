/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package trabalhopratico;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.opencsv.exceptions.CsvException;

import trabalhopratico.Classes.ClientScoreManagement;
import trabalhopratico.Classes.ImporterExporterFiles;
import trabalhopratico.Classes.ReservationManagement;
import trabalhopratico.Classes.StatisticsGeral;

public class ImportTest {
    private ImporterExporterFiles importerExporterFiles;

    private ClientScoreManagement clients;

    private ReservationManagement reservations;

    @DisplayName("Import data from a valid csv file")
    @Test
    public void testImporter_StoreDataInArrayList_Valid() {
        File file = new File("Files/Dataset_nova_versao.csv");
        String path = file.getAbsolutePath();

        importerExporterFiles = new ImporterExporterFiles();

        clients = new ClientScoreManagement();

        reservations = new ReservationManagement();

        String expected = "Successful";
        
        try {
            Assertions.assertEquals(expected, (importerExporterFiles.importer(path, clients.getClients(), reservations.getReservas())));
        } catch (IOException | CsvException | ParseException e) {
            e.printStackTrace();
        }
    }

    @DisplayName("Import data from an invalid file")
    @Test
    public void testImporter_ReturnFileNotFoundException_Valid() throws IOException, CsvException, ParseException {
        File file = new File("");
        String path = file.getAbsolutePath();

        importerExporterFiles = new ImporterExporterFiles();

        clients = new ClientScoreManagement();

        reservations = new ReservationManagement();
        
        Assertions.assertThrows(java.io.FileNotFoundException.class, () -> importerExporterFiles.importer(path, clients.getClients(), reservations.getReservas()));
    }

    @DisplayName("Import data from an empty csv file")
    @Test
    public void testImporter_ReturnIllegalArgumentException_Valid1() {
        File file = new File("Files/EmptyCsv.csv");
        String path = file.getAbsolutePath();
        
        importerExporterFiles = new ImporterExporterFiles();

        clients = new ClientScoreManagement();

        reservations = new ReservationManagement();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class, () -> importerExporterFiles.importer(path, clients.getClients(), reservations.getReservas()));
    }

    @DisplayName("Import data from an csv file with wrong formatted data")
    @Test
    public void testImporter_ReturnIllegalArgumentException_Valid2() {
        File file = new File("Files/WrongFormat.csv");
        String path = file.getAbsolutePath();

        importerExporterFiles = new ImporterExporterFiles();

        clients = new ClientScoreManagement();

        reservations = new ReservationManagement();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class, () -> importerExporterFiles.importer(path, clients.getClients(), reservations.getReservas()));
    }

    @DisplayName("Export data to a JSON file")
    @Test
    public void testExporter_ReturnSucessful_Valid() {
        File file = new File("Files/Estatisticas.json");
        String pathExporter = file.getAbsolutePath();

        file = new File("Files/Dataset_nova_versao.csv");
        String pathImporter = file.getAbsolutePath();

        importerExporterFiles = new ImporterExporterFiles();

        clients = new ClientScoreManagement();

        reservations = new ReservationManagement();

        try {
            importerExporterFiles.importer(pathImporter, clients.getClients(), reservations.getReservas());
        } catch (IOException | CsvException | ParseException e) {
        }

        StatisticsGeral statistics = new StatisticsGeral();

        clients.removeDuplicates();

        statistics.assignEverything(clients.getClients(), reservations.getReservas());

        clients.addMonetizacaoScore();
        clients.addRegularidadeScore();
        clients.addTotalComprasScore();

        statistics.getClientBetterAverageScore(clients.getClients());
        statistics.getClientWorstAverageScore(clients.getClients());

        String expected = "Successful";

        try {
            Assertions.assertEquals(expected,importerExporterFiles.exporter(pathExporter, clients.getClients(), statistics));
        } catch (IOException e) {
        }
    }

    @DisplayName("Export data to a JSON file using an invalid filename")
    @Test
    public void testExporter_ReturnIOException_Valid() {
        File file2 = new File("Files/Dataset_nova_versao.csv");
        String pathImporter = file2.getAbsolutePath();

        importerExporterFiles = new ImporterExporterFiles();

        clients = new ClientScoreManagement();

        reservations = new ReservationManagement();

        try {
            importerExporterFiles.importer(pathImporter, clients.getClients(), reservations.getReservas());
        } catch (IOException | CsvException | ParseException e) {
        }

        StatisticsGeral statistics = new StatisticsGeral();

        Assertions.assertThrows(java.io.IOException.class, () -> importerExporterFiles.exporter("", clients.getClients(), statistics));
    }

    @DisplayName("Export data to a JSON file without importing beforehand")
    @Test
    public void testExporter_ReturnIllegalArgumentException_Valid() {
        File file = new File("Files/Estatisticas.json");
        String pathExporter = file.getAbsolutePath();

        importerExporterFiles = new ImporterExporterFiles();

        clients = new ClientScoreManagement();

        reservations = new ReservationManagement();

        StatisticsGeral statistics = new StatisticsGeral();

        Assertions.assertThrows(java.lang.IllegalArgumentException.class, () -> importerExporterFiles.exporter(pathExporter, clients.getClients(), statistics));
    }
}
