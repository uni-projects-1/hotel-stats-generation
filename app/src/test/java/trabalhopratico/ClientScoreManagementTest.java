package trabalhopratico;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.opencsv.exceptions.CsvException;

import trabalhopratico.Classes.Client;
import trabalhopratico.Classes.ClientScoreManagement;
import trabalhopratico.Classes.ImporterExporterFiles;
import trabalhopratico.Classes.ReservationManagement;


public class ClientScoreManagementTest {
    private ClientScoreManagement clients;

    private ReservationManagement reservations;

    private ImporterExporterFiles importer;

    public ClientScoreManagementTest() {
        clients = new ClientScoreManagement();
        reservations = new ReservationManagement();
        importer = new ImporterExporterFiles();
    }

    public void importBeforeTesting() {
        File file = new File("Files/Dataset_nova_versao.csv");
        String path = file.getAbsolutePath();

        try {
            importer.importer(path, clients.getClients(), reservations.getReservas());
        } catch (IOException | CsvException | ParseException e) {
            
        }

        clients.removeDuplicates();
    }

    //#region ADICIONAR SCORE DE MONETIZAÇÃO

    @DisplayName("Adicionar score de monetizacao a todos os clientes de uma lista")
    @Test
    public void testAddMonetizacaoScore_ReturnSuccessful_Valid() {
        this.importBeforeTesting();
        String expected = "Sucessful";

        Assertions.assertEquals(expected, clients.addMonetizacaoScore());
    }


    @DisplayName("Adicionar score de monetizacao a todos os clientes de uma lista vazia")
    @Test
    public void testAddMonetizacaoScore_ReturnIllegalArgumentException_Valid() {
        Assertions.assertThrows(java.lang.IllegalArgumentException.class, () -> clients.addMonetizacaoScore());
    }    

    //#endregion

    //#region ADICIONAR SCORE DE REGULARIDADE

    @DisplayName("Adicionar score de regularidade a todos os clientes de uma lista")
    @Test
    public void testAddRegularidadeScore_ReturnSuccessful_Valid() {
        this.importBeforeTesting();
        String expected = "Successful";

        Assertions.assertEquals(expected, clients.addRegularidadeScore());
    }


    @DisplayName("Adicionar score de regularidade a todos os clientes de uma lista vazia")
    @Test
    public void testAddRegularidadeScore_ReturnIllegalArgumentException_Valid() {
        Assertions.assertThrows(java.lang.IllegalArgumentException.class, () -> clients.addRegularidadeScore());
    }

    //#endregion


     //#region ADICIONAR SCORE DE TOTAL DE COMPRAS

     @DisplayName("Adicionar score de total de compras(reservas) a todos os clientes de uma lista")
     @Test 
     public void testAddTotalComprasScore_ReturnSuccessful_Valid() {
         this.importBeforeTesting();
         String expected = "Sucessful";
 
         Assertions.assertEquals(expected, clients.addTotalComprasScore());
     }
 
 
     @DisplayName("Adicionar score de total de compras (reservas) a todos os clientes de uma lista vazia")
     @Test
     public void testAddTotalComprasScore_ReturnIllegalArgumentException_Valid() {
         Assertions.assertThrows(java.lang.IllegalArgumentException.class, () -> clients.addTotalComprasScore());
     }    
 
     //#endregion
}
