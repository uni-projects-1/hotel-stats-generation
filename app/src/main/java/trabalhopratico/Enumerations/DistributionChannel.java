package trabalhopratico.Enumerations;

//Enumeration type class that has all possible and accepted values for distribution channel
public enum DistributionChannel {
    TRAVEL_AGENT_OPERATOR("Travel Agent/Operator"),
    DIRECT("Direct"),
    CORPORATE("Corporate"),
    ELECTRONIC_DISTRIBUTION("Electronic Distribution");

    private String s;

    //Constructor Method: Creat instances of DistributionChannel
    DistributionChannel(String s){
        this.s = s;
    }

    public String getDistributionChannel() {
        return this.s;
    }
}
