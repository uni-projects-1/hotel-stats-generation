package trabalhopratico.Enumerations;

/**
 * Enumeration type class that has all possible and accepted values for market segment
 */
public enum MarketSegment {
    DIRECT("Direct"), 
    GROUPS("Groups"),
    AVIATION("Aviation"), 
    COMPLEMENTARY("Complementary"), 
    OTHER("Other"), 
    TRAVEL_AGENT_OPERATOR("Travel Agent/Operator"),
    CORPORATE("Corporate");


    private String s;
    
    //Constructor Method: Creat instances of MarketSegment
    MarketSegment(String s){
        this.s = s;
    }

    public String getMarketSegment() {
        return this.s;
    }

}
