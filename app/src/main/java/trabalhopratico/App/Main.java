package trabalhopratico.App;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import com.opencsv.exceptions.CsvException;

import trabalhopratico.Classes.ClientScoreManagement;
import trabalhopratico.Classes.ImporterExporterFiles;
import trabalhopratico.Classes.ReservationManagement;
import trabalhopratico.Classes.StatisticsGeral;

public class Main {
        public static void main(String[] args) {
                // instânciar as classes
                ClientScoreManagement clients = new ClientScoreManagement();
                ReservationManagement reservations = new ReservationManagement();
                ImporterExporterFiles importer = new ImporterExporterFiles();
                StatisticsGeral statistics = new StatisticsGeral();

                importData(importer, reservations, clients);
                //printAllStatistics(statistics, reservations, clients);
                //System.out.println("" + clients.getClients().get(80200).toString());
        }

        /**
         * importar dados do csv e colocar os dados nos clientes e nas suas reservas
         */
        private static void importData(ImporterExporterFiles importer,
                        ReservationManagement reservations, ClientScoreManagement clients) {
                // importar dados do csv e colocar os dados nos clientes
                // e nas suas reservas

                // obter o ficheiro csv
                File file = new File("Files/Dataset_nova_versao.csv");
                String path = file.getAbsolutePath();

                try {
                        importer.importer(path, clients.getClients(), reservations.getReservas());
                } catch (IOException | CsvException | ParseException e) {
                        e.printStackTrace();
                }

                // remover os dados duplicados que vieram do csv (clientes repetidos, coloca
                // apenas um e associa todas as reservas ao mesmo)
                clients.removeDuplicates();
        }

        /**
         * obter as estatisticas
         */
        private static void printAllStatistics(StatisticsGeral statistics, ReservationManagement reservations,
                        ClientScoreManagement clients) {
                // obter as estatisticas

                // canal de distribuicao preferida no geral
                System.out.println("Distribution Channel prefered: "
                                + statistics.getDistributionChannelGeneral(reservations.getReservas()));

                System.out.println("\n");

                // intervalo minimo entre compras no geral e do cliente
                System.out.println("Minimo intervalo: "
                                + statistics.getMinIntervalTimeBetweenPurchasesClient(clients.getClients().get(20)));
                System.out.println("Intervalo minimo entre compras geral: "
                                + statistics.getMinIntervalTimeBetweenPurchasesGeneral(clients.getClients()));

                System.out.println("\n");

                // intervalo maximo entre compras no geral e do cliente
                System.out.println("Intervalo maximo entre compras geral: "
                                + statistics.getMaxIntervalTimeBetweenPurchasesGeneral(clients.getClients()));
                System.out.println("Max do intervalo de tempo entre compras do cliente: "
                                + statistics.getMaxIntervalTimeBetweenPurchasesClient(clients.getClients().get(20)));

                System.out.println("\n");

                // media de compras no geral
                System.out.println(
                                "Media de compras de todos os cliente: "
                                                + statistics.getAverageValueReservation(reservations.getReservas()));

                System.out.println("\n");

                // estacao do ano preferida para efetuar compras no geral e do cliente
                System.out.println("Estacao do ano preferida no geral: "
                                + statistics.getPreferedSeasonalityGeneral(reservations.getReservas()));
                System.out.println("Estacao do ano preferida do cliente: "
                                + statistics.getPreferedSeasonalityByClient(clients.getClients().get(20)));

                System.out.println("\n");

                // intervalo medio entre compras no geral e do cliente
                System.out.println("Media do intervalo de tempo entre compras do cliente: "
                                + statistics.getAverageIntervalTimeBetweenPurchasesByClient(
                                                clients.getClients().get(20)));
                System.out.println("Media do intervalo de tempo entre compras no geral: "
                                + statistics.getAverageIntervalTimeBetweenPurchasesGeneral(reservations.getReservas()));

                System.out.println("\n");

                // metodo de pagamento preferido no geral
                System.out.println(
                                "Metodo de pagamento: "
                                                + statistics.getPaymentMethodGeneral(reservations.getReservas()));


                /*
                 * for(int i=0; i < clientManagement.getClients().size(); i++)
                 * {
                 * System.out.println(clientManagement.getClients().get(i));
                 * System.out.println("\n");
                 * }
                 */

                /**
                 * for (int i = 0; i < clientManagement.getClients().size(); i++) {
                 * if (clientManagement.getClients().get(i).getReservations().size() > 100) {
                 * System.out.println("" +
                 * clientManagement.getClients().get(i).getReservations().get(100).toString());
                 * }
                 * }
                 */

                System.out.println("\n");

                // cliente com maior e menor regularidade
                System.out.println("Cliente com maior regularidade: \n"
                                + statistics.getClientMaxRegularity(clients.getClients()));

                System.out.println("Cliente com menor regularidade: \n"
                                + statistics.getClientMinRegularity(clients.getClients()));

                System.out.println("\n");

                // score regularidade
                clients.addRegularidadeScore();
                clients.addMonetizacaoScore();
                clients.addTotalComprasScore();

                System.out.println("\n");

                // cliente com melhor e pior score
                // System.out.println("Cliente com maior regularidade: \n"
                // + statistics.getClientMaxRegularity(clients.getClients()));

                System.out.println("\n");

                System.out.println("Cliente com melhor score: \n\n\n"
                                + statistics.getClientBetterAverageScore(clients.getClients()));
                
                System.out.println("\n");

                        // cliente menor total de compras

                System.out.println("\n");
               
                System.out.println("Cliente com menor total de compras:\n"
                                + statistics.getClientMinTotalBookings(clients.getClients()));

                System.out.println("\n");

                //System.out.println("\n");

                //System.out.println("Cliente com maior total de compras: \n\n\n"
                               // + statistics.getClientMaxTotalBookings(clients.getClients()));

               // System.out.println("\n");

        }
}
