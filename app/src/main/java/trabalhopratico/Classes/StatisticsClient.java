package trabalhopratico.Classes;


import com.google.gson.annotations.Expose;

/**
 * class that will store all the unique statistics of each client
 */
public class StatisticsClient {

    // Score to be attributed based on how often the client makes a reservation
    @Expose
    private int regularidadeScore;

    // Score to be attributed based on how many reservations the client has made
    @Expose
    private int totalComprasScore;

    // Score to be attributed based on how much money the client has spent on
    // reservations
    @Expose
    private int monetizacaoScore;

    // The client´s prefered season of the year to make reservations
    @Expose
    private String seasonalityPreference;

    // The client´s prefered payment method used when making reservations
    @Expose
    private int paymentMethodPreference;

    // The maximum amount of time between making each reservation
    @Expose
    private long maxIntervalTimeBetweenPurchases;

    // The minimum amount of time between making each reservation
    @Expose
    private long minIntervalTimeBetweenPurchases;

    // The average amount of time between making each reservation
    @Expose
    private double meanIntervalTimeBetweenPurchases;

    /**
     * Constructor : create instances of type StatisticsClient
     */
    public StatisticsClient() {
    }

    /**
     * Gets the attribute {@link StatisticsClient#regularidadeScore
     * regularidadeScore}
     * 
     * @return the item {@link StatisticsClient#regularidadeScore regularidadeScore}
     */
    public int getRegularidadeScore() {
        return regularidadeScore;
    }

    /**
     * Gets the attribute {@link StatisticsClient#totalComprasScore
     * totalComprasScore}
     * 
     * @return the item {@link StatisticsClient#totalComprasScore totalComprasScore}
     */
    public int getTotalComprasScore() {
        return totalComprasScore;
    }

    /**
     * Gets the attribute {@link StatisticsClient#monetizacaoScore monetizacaoScore}
     * 
     * @return The item {@link StatisticsClient#monetizacaoScore monetizacaoScore}
     */
    public int getMonetizacaoScore() {
        return monetizacaoScore;
    }

    /**
     * Gets the attribute {@link StatisticsClient#seasonalityPreference
     * seasonalityPreference}
     * 
     * @return The item {@link StatisticsClient#seasonalityPreference
     *         seasonalityPreference}
     */
    public String getSeasonalityPreference() {
        return seasonalityPreference;
    }

    /**
     * Gets the attribute {@link StatisticsClient#paymentMethodPreference
     * paymentMethodPreference}
     * 
     * @return The item {@link StatisticsClient#paymentMethodPreference
     *         paymentMethodPreference}
     */
    public int getPaymentMethodPreference() {
        return paymentMethodPreference;
    }

    /**
     * Gets the attribute {@link StatisticsClient#maxIntervalTimeBetweenPurchases
     * maxIntervalTimeBetweenPurchases}
     * 
     * @return The item {@link StatisticsClient#maxIntervalTimeBetweenPurchases
     *         maxIntervalTimeBetweenPurchases}
     */
    public long getMaxIntervalTimeBetweenPurchases() {
        return maxIntervalTimeBetweenPurchases;
    }

    /**
     * Gets the attribute {@link StatisticsClient#minIntervalTimeBetweenPurchases
     * minIntervalTimeBetweenPurchases}
     * 
     * @return The item {@link StatisticsClient#minIntervalTimeBetweenPurchases
     *         minIntervalTimeBetweenPurchases}
     */
    public long getMinIntervalTimeBetweenPurchases() {
        return minIntervalTimeBetweenPurchases;
    }

    /**
     * 
     * @return
     */
    public double getMeanIntervalTimeBetweenPurchases() {
        return meanIntervalTimeBetweenPurchases;
    }

    /**
     * 
     * @param regularidadeScore
     */
    public void setRegularidadeScore(int regularidadeScore) {
        this.regularidadeScore = regularidadeScore;
    }

    /**
     * 
     * @param totalComprasScore
     */
    public void setTotalComprasScore(int totalComprasScore) {
        this.totalComprasScore = totalComprasScore;
    }

    /**
     * 
     * @param monetizacaoScore
     */
    public void setMonetizacaoScore(int monetizacaoScore) {
        this.monetizacaoScore = monetizacaoScore;
    }

    /**
     * 
     * @param seasonalityPreference
     */
    public void setSeasonalityPreference(String seasonalityPreference) {
        this.seasonalityPreference = seasonalityPreference;
    }

    /**
     * 
     * @param paymentMethodPreference
     */
    public void setPaymentMethodPreference(int paymentMethodPreference) {
        this.paymentMethodPreference = paymentMethodPreference;
    }

    /**
     * 
     * @param maxIntervalTimeBetweenPurchases
     */
    public void setMaxIntervalTimeBetweenPurchases(long maxIntervalTimeBetweenPurchases) {
        this.maxIntervalTimeBetweenPurchases = maxIntervalTimeBetweenPurchases;
    }

    /**
     * 
     * @param minIntervalTimeBetweenPurchases
     */
    public void setMinIntervalTimeBetweenPurchases(long minIntervalTimeBetweenPurchases) {
        this.minIntervalTimeBetweenPurchases = minIntervalTimeBetweenPurchases;
    }

    /**
     * 
     * @param meanIntervalTimeBetweenPurchases
     */
    public void setMeanIntervalTimeBetweenPurchases(double meanIntervalTimeBetweenPurchases) {
        this.meanIntervalTimeBetweenPurchases = meanIntervalTimeBetweenPurchases;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        return "StatisticsClient [regularidadeScore=" + regularidadeScore + ", totalComprasScore=" + totalComprasScore
                + ", monetizacaoScore=" + monetizacaoScore + ", seasonalityPreference=" + seasonalityPreference
                + ", paymentMethodPreference=" + paymentMethodPreference
                + ", maxIntervalTimeBetweenPurchases=" + maxIntervalTimeBetweenPurchases
                + ", minIntervalTimeBetweenPurchases=" + minIntervalTimeBetweenPurchases
                + ", meanIntervalTimeBetweenPurchases=" + meanIntervalTimeBetweenPurchases + "]";
    }

}

