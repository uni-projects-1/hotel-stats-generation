package trabalhopratico.Classes;

import java.time.LocalDate;
import java.util.ArrayList;

import trabalhopratico.Enumerations.DistributionChannel;
import trabalhopratico.Enumerations.MarketSegment;

/**
 * Reservation class represents the reservation made by the customer and contains information
 * both on the current reservation and on previous reservations
 */

public class Reservation {
    // Identification code that uniquely and exclusively references a reservation
    private int idReserva;

    // Integer number that represents the number of people per night of the reservation
    private int personsNight;

    // Integer number representing the number of rooms booked per night
    private int roomNights;

    // Integer representing the days since the first stay
    private int daysSinceFirstStay;

    // Integer representing the days since the last stay
    private int daysSinceLastStay;

    // Amount received from the customer´s last stay
    private double lodgingRevenue;

    // Amount received from other requested services
    private double otherRevenue;

    // Current stay date
    private LocalDate purchasesDate;

    // Method used to pay for the stay
    private int paymentMethod;

    // Distribution channel used to book the stay
    private DistributionChannel distributionChannel;

    // Market Segment to which the customer belongs
    private MarketSegment marketSegment;

    // Reservation preferences type object that stores the choices made by the customer for this reservation
    //private ReservationPreferences preferenceReservation;
    private ArrayList<ReservationPreferences> preferenceReservation;
    
    /**
     * @param idReserva the {@link Reservation#idReserva idReserva } 
     * @param personsNight the {@link Reservation#personsNight personsNight}
     * @param roomNights the {@link Reservation#roomNights roomNights}
     * @param averageLeadTime the {@link Reservation#averageLeadTime averageLeadTime}
     * @param daysSinceFirstStay the {@link Reservation#daysSinceFirstStay daysSinceFirstStay}
     * @param daysSinceLastStay the {@link Reservation#daysSinceLastStay daysSinceLastStay}
     * @param lodgingRevenue the {@link Reservation#lodgingRevenue lodgingRevenue}
     * @param otherRevenue the {@link Reservation#otherRevenue otherRevenue}
     * @param purchasesDate the {@link Reservation#purchasesDate purchasesDate}
     * @param paymentMethod the {@link Reservation#paymentMethod paymentMethod}
     * @param distributionChannel the {@link Reservation#distributionChannel distributionChannel}
     * @param marketSegment {@link Reservation#marketSegment marketSegment}
     */
    public Reservation(int idReserva, int personsNight, int roomNights, int daysSinceFirstStay,
            int daysSinceLastStay, double lodgingRevenue, double otherRevenue, LocalDate purchasesDate, int paymentMethod,
            DistributionChannel distributionChannel, MarketSegment marketSegment) {
        this.idReserva = idReserva;
        this.personsNight = personsNight;
        this.roomNights = roomNights;
        this.daysSinceFirstStay = daysSinceFirstStay;
        this.daysSinceLastStay = daysSinceLastStay;
        this.lodgingRevenue = lodgingRevenue;
        this.otherRevenue = otherRevenue;
        this.purchasesDate = purchasesDate;
        this.paymentMethod = paymentMethod;
        this.distributionChannel = distributionChannel;
        this.marketSegment = marketSegment;
        this.preferenceReservation = new ArrayList<>();
    }

    /**
     * Gets the attribute {@link Reservation#idReserva idReserva }
     * 
     * @return the item {@link Reservation#idReserva idReserva}
     */
    public int getIdReserva() {
        return idReserva;
    }

    /**
     * Gets the attribute {@link Reservation#personsNight personsNight}
     * 
     * @return the item {@link Reservation#personsNight personsNight}
     */
    public int getPersonsNight() {
        return personsNight;
    }

    /**
     * Gets the attribute {@link Reservation#roomNights roomNights}
     * 
     * @return the item {@link Reservation#roomNights roomNights}
     */
    public int getRoomNights() {
        return roomNights;
    }

    /**
     * Gets the attribute {@link Reservation#daysSinceFirstStay daysSinceFirstStay}
     * 
     * @return the item {@link Reservation#daysSinceFirstStay daysSinceFirstStay}
     */
    public int getDaysSinceFirstStay() {
        return daysSinceFirstStay;
    }

    /**
     * Gets the attribute {@link Reservation#daysSinceLastStay daysSinceLastStay}
     * 
     * @return the item {@link Reservation#daysSinceLastStay daysSinceLastStay}
     */
    public int getDaysSinceLastStay() {
        return daysSinceLastStay;
    }

    /**
     * Gets the attribute {@link Reservation#lodgingRevenue lodgingRevenue}
     * 
     * @return the item {@link Reservation#lodgingRevenue lodgingRevenue}
     */
    public double getLodgingRevenue() {
        return lodgingRevenue;
    }

    /**
     * Gets the attribute {@link Reservation#otherRevenue otherRevenue}
     * 
     * @return the item {@link Reservation#otherRevenue otherRevenue}
     */
    public double getOtherRevenue() {
        return otherRevenue;
    }

    /**
     * Gets the attribute {@link Reservation#purchasesDate purchasesDate}
     * 
     * @return the item {@link Reservation#purchasesDate purchasesDate}
     */
    public LocalDate getPurchasesDate() {
        return purchasesDate;
    }

    /**
     * Gets the attribute {@link Reservation#paymentMethod paymentMethod}
     * 
     * @return the item {@link Reservation#paymentMethod paymentMethod}
     */
    public int getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Gets the attribute {@link Reservation#distributionChannel distributionChannel}
     * 
     * @return the item {@link Reservation#distributionChannel distributionChannel}
     */
    public DistributionChannel getDistributionChannel() {
        return distributionChannel;
    }

    /**
     * Gets the attribute {@link Reservation#marketSegment marketSegment}
     * 
     * @return the item {@link Reservation#marketSegment marketSegment}
     */
    public MarketSegment getMarketSegment() {
        return marketSegment;
    }

    /**
     * Gets the attribute {@link Reservation#preferenceReservation preferenceReservation}
     * 
     * @return the item {@link Reservation#preferenceReservation preferenceReservation}
     */
    public ArrayList<ReservationPreferences> getPreferenceReservation() {
        return this.preferenceReservation;
    }

    /**
     * Sets the attribute {@link Reservation#idReserva idReserva}
     * 
     * @param idReserva the {@link Reservation#idReserva idReserva}
     */
    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }

    /**
     * Sets the attribute {@link Reservation#personsNight personsNight}
     * 
     * @param personsNight the {@link Reservation#personsNight personsNight}
     */
    public void setPersonsNight(int personsNight) {
        this.personsNight = personsNight;
    }

    /**
     * Sets the attribute {@link Reservation#roomNights roomNights}
     * 
     * @param roomNights the {@link Reservation#roomNights roomNights}
     */
    public void setRoomNights(int roomNights) {
        this.roomNights = roomNights;
    }

    /**
     * Sets the attribute {@link Reservation#daysSinceFirstStay daysSinceFirstStay}
     * 
     * @param daysSinceFirstStay the {@link Reservation#daysSinceFirstStay daysSinceFirstStay}
     */
    public void setDaysSinceFirstStay(int daysSinceFirstStay) {
        this.daysSinceFirstStay = daysSinceFirstStay;
    }

    /**
     * Sets the attribute {@link Reservation#daysSinceLastStay daysSinceLastStay}
     * 
     * @param daysSinceLastStay the {@link Reservation#daysSinceLastStay daysSinceLastStay}
     */
    public void setDaysSinceLastStay(int daysSinceLastStay) {
        this.daysSinceLastStay = daysSinceLastStay;
    }

    /**
     * Sets the attribute {@link Reservation#lodgingRevenue lodgingRevenue}
     * 
     * @param lodgingRevenue the {@link Reservation#lodgingRevenue lodgingRevenue}
     */
    public void setLodgingRevenue(double lodgingRevenue) {
        this.lodgingRevenue = lodgingRevenue;
    }

    /**
     * Sets the attribute {@link Reservation#otherRevenue otherRevenue}
     * 
     * @param otherRevenue {@link Reservation#otherRevenue otherRevenue}
     */
    public void setOtherRevenue(double otherRevenue) {
        this.otherRevenue = otherRevenue;
    }

    /**
     * Sets the attribute {@link Reservation#purchasesDate purchasesDate}
     * 
     * @param purchasesDate the {@link Reservation#purchasesDate purchasesDate}
     */
    public void setPurchasesDate(LocalDate purchasesDate) {
        this.purchasesDate = purchasesDate;
    }

    /**
     * Sets the attribute {@link Reservation#paymentMethod paymentMethod}
     * 
     * @param paymentMethod the {@link Reservation#paymentMethod paymentMethod}
     */
    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * Sets the attribute {@link Reservation#distributionChannel distributionChannel}
     * 
     * @param distributionChannel the {@link Reservation#distributionChannel distributionChannel}
     */
    public void setDistributionChannel(DistributionChannel distributionChannel) {
        this.distributionChannel = distributionChannel;
    }

    /**
     * Sets the attribute {@link Reservation#marketSegment marketSegment}
     * 
     * @param marketSegment the {@link Reservation#marketSegment marketSegment}
     */
    public void setMarketSegment(MarketSegment marketSegment) {
        this.marketSegment = marketSegment;
    }

    /**
     * Returns a string representation of the object. In general, the toString
     * method returns a string that "textually represents" this object. The
     * result should be a concise but informative representation that is easy
     * for a person to read
     * 
     * @return A string representation of the object (<code>Reservation</code>)
     */
    @Override
    public String toString() {
        return "Reservation [idReserva=" + idReserva + ", personsNight=" + personsNight + ", roomNights=" + roomNights
                + ", daysSinceFirstStay=" + daysSinceFirstStay
                + ", daysSinceLastStay=" + daysSinceLastStay + ", lodgingRevenue=" + lodgingRevenue + ", otherRevenue="
                + otherRevenue + ", purchasesDate=" + purchasesDate + ", paymentMethod=" + paymentMethod
                + ", distributionChannel=" + distributionChannel + ", marketSegment=" + marketSegment
                + ", preferenceReservation=" + preferenceReservation + "]";
    }
}
