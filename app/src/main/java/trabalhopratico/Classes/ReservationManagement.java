package trabalhopratico.Classes;

import java.util.ArrayList;

/**
 * Class that holds all reservations made
 */
public class ReservationManagement {
    // Array that holds reservation type instances
    private ArrayList<Reservation> reservations;

    /*
     * Constructor for this list of reservations using an array list
     */
    public ReservationManagement() {
        this.reservations = new ArrayList<>();
    }

    /**
     * Returns the array of reservations
     * 
     * @return the array of reservations
     */
    public ArrayList<Reservation> getReservas() {
        return this.reservations;
    }

    


}
