package trabalhopratico.Classes;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;

import com.google.gson.annotations.Expose;
import trabalhopratico.Enumerations.DistributionChannel;
import trabalhopratico.Interfaces.IStatistics;

public class StatisticsGeral implements IStatistics {

    // Time of year preferred by customers in general to make reservations
    @Expose
    private String SeasonalityGeneral;

    // Distribution channel most used by customers in general to make their
    // reservations
    @Expose
    private DistributionChannel distributionChannelGeneral;

    // Average monetary value received from hotel reservations
    @Expose
    private double averageValueReservation;

    // Minimum monetary amount received from hotel reservations
    @Expose
    private double minValueReservation;

    // Maximum monetary value received from hotel reservations
    @Expose
    private double maxValueReservation;

    // Payment method most used by customers in general to make their purchases
    @Expose
    private int paymentMethodGeneral;

    // Average time gap between hotel bookings
    @Expose
    private double averageIntervalTimeBetweenPurchasesGeneral;

    // Maximum time gap between hotel reservations
    @Expose
    private long maxIntervalTimeBetweenPurchasesGeneral;

    // Minimum time gap between hotel reservations
    @Expose
    private long minIntervalTimeBetweenPurchasesGeneral;

    // Customer with the maximum monetary amount spent on bookings
    @Expose
    private Client clientMaxValueMonetization;

    // Customer with the minimum monetary amount spent on reservations
    @Expose
    private Client clientMinValueMonetization;

    // Customer with greater regularity in hotel reservations
    @Expose
    private Client clientMaxRegularity;

    // Customer with less regular bookings at the hotel
    @Expose
    private Client clientMinRegularity;

    // Customer with the highest number of reservations made at the hotel
    @Expose
    private Client clientMaxTotalBookings;

    // Customer with the lowest number of reservations made at the hotel
    @Expose
    private Client clientMinTotalBookings;

    // Customer with the best average score
    @Expose
    private Client clientBetterAverageScore;

    // Customer with the worst average score
    @Expose
    private Client clientWorstAverageScore;

    // #region PREFERED SEASONALITY

    /**
     * Qual a altura do ano preferida no geral (p.ex. Outono, Inverno, Primavera ou
     * Verão)
     * 
     * @throws ParseException
     */
    @Override
    public String getPreferedSeasonalityGeneral(ArrayList<Reservation> reservations) {

        // #region VARIAVEIS

        int countOutono = 0, countInverno = 0, countPrimavera = 0, countVerao = 0;

        LocalDate newPurchaseDate;
        LocalDate outono1 = LocalDate.of(0, 9, 23);
        LocalDate outono2 = LocalDate.of(0, 12, 21);
        LocalDate inverno1 = LocalDate.of(0, 12, 21);
        LocalDate inverno2 = LocalDate.of(0, 3, 20);
        LocalDate primavera1 = LocalDate.of(0, 3, 20);
        LocalDate primavera2 = LocalDate.of(0, 6, 21);
        LocalDate verao1 = LocalDate.of(0, 6, 21);
        LocalDate verao2 = LocalDate.of(0, 9, 23);

        String season = "";

        // #endregion

        if (reservations.isEmpty()) {
            throw new IllegalArgumentException("O array de reservas está vazio!!!");
        }

        for (int i = 0; i < reservations.size(); i++) {
            newPurchaseDate = LocalDate.of(0, reservations.get(i).getPurchasesDate().getMonthValue(),
                    reservations.get(i).getPurchasesDate().getDayOfMonth());

            // verificar a época que o cliente reservou
            if (newPurchaseDate.isAfter(outono1) && newPurchaseDate.isBefore(outono2)) {
                countOutono += 1;
            }

            if (newPurchaseDate.isAfter(inverno1) && newPurchaseDate.isBefore(inverno2)) {
                countInverno += 1;
            }

            if (newPurchaseDate.isAfter(primavera1) && newPurchaseDate.isBefore(primavera2)) {
                countPrimavera += 1;

            } else {
                countVerao += 1;
            }
        }

        if (countOutono > countInverno && countOutono > countPrimavera && countInverno > countVerao) {
            season = "Outono";
        }

        if (countInverno > countOutono && countInverno > countPrimavera && countInverno > countVerao) {
            season = "Inverno";
        }

        if (countVerao > countInverno && countVerao > countPrimavera && countVerao > countOutono) {
            season = "Verao";
        }

        else {
            season = "Primavera";
        }

        this.SeasonalityGeneral = season;

        return this.SeasonalityGeneral;
    }

    /**
     * Qual a altura do ano preferida de cada cliente (p.ex. Outono, Inverno,
     * Primavera ou Verão)
     */
    @Override
    public String getPreferedSeasonalityByClient(Client client) {

        // #region VARIAVEIS

        int countOutono = 0, countInverno = 0, countPrimavera = 0, countVerao = 0;

        LocalDate newPurchaseDate;
        LocalDate outono1 = LocalDate.of(0, 9, 23);
        LocalDate outono2 = LocalDate.of(0, 12, 21);
        LocalDate inverno1 = LocalDate.of(0, 12, 21);
        LocalDate inverno2 = LocalDate.of(0, 3, 20);
        LocalDate primavera1 = LocalDate.of(0, 3, 20);
        LocalDate primavera2 = LocalDate.of(0, 6, 21);
        LocalDate verao1 = LocalDate.of(0, 6, 21);
        LocalDate verao2 = LocalDate.of(0, 9, 23);

        String season = "";

        // #endregion

        if (client.getReservations().isEmpty()) {
            throw new IllegalArgumentException("O array de reservas está vazio!!!");
        }

        for (int i = 0; i < client.getReservations().size(); i++) {
            newPurchaseDate = LocalDate.of(0, client.getReservations().get(i).getPurchasesDate().getMonthValue(),
                    client.getReservations().get(i).getPurchasesDate().getDayOfMonth());

            // verificar a época que o cliente reservou
            if (newPurchaseDate.isAfter(outono1) && newPurchaseDate.isBefore(outono2)) {
                countOutono += 1;
            }

            if (newPurchaseDate.isAfter(inverno1) && newPurchaseDate.isBefore(inverno2)) {
                countInverno += 1;
            }

            if (newPurchaseDate.isAfter(primavera1) && newPurchaseDate.isBefore(primavera2)) {
                countPrimavera += 1;

            } else {
                countVerao += 1;
            }
        }

        if (countOutono > countInverno && countOutono > countPrimavera && countInverno > countVerao) {
            season = "Outono";
        }

        if (countInverno > countOutono && countInverno > countPrimavera && countInverno > countVerao) {
            season = "Inverno";
        }

        if (countVerao > countInverno && countVerao > countPrimavera && countVerao > countOutono) {
            season = "Verao";
        }

        else {
            season = "Primavera";
        }

        client.getEstatisticasCliente().setSeasonalityPreference(season);

        return client.getEstatisticasCliente().getSeasonalityPreference();
    }

    // #endregion

    // #region DISTRIBUTION CHANNEL

    /**
     * Qual o distribution channel mais utilizado no geral para fazer a reserva
     */
    @Override
    public DistributionChannel getDistributionChannelGeneral(ArrayList<Reservation> reservations) {
        DistributionChannel channelPrefered;
        int count_Travel_Agent_Operator = 0;
        int count_Direct = 0;
        int count_Corporate = 0;
        int count_Electronic_Distribution = 0;

        if (reservations.isEmpty()) {
            throw new IndexOutOfBoundsException("O array de clientes está vazio!!!");
        }

        for (int i = 0; i < reservations.size(); i++) {
            if (reservations.get(i).getDistributionChannel().equals(DistributionChannel.TRAVEL_AGENT_OPERATOR)) {
                count_Travel_Agent_Operator++;
            }

            if (reservations.get(i).getDistributionChannel().equals(DistributionChannel.DIRECT)) {
                count_Direct++;
            }

            if (reservations.get(i).getDistributionChannel().equals(DistributionChannel.CORPORATE)) {
                count_Corporate++;
            }

            if (reservations.get(i).getDistributionChannel().equals(DistributionChannel.ELECTRONIC_DISTRIBUTION)) {
                count_Electronic_Distribution++;
            }
        }

        if (count_Travel_Agent_Operator > count_Direct && count_Travel_Agent_Operator > count_Corporate
                && count_Travel_Agent_Operator > count_Electronic_Distribution) {
            channelPrefered = DistributionChannel.TRAVEL_AGENT_OPERATOR;
        } else if (count_Direct > count_Travel_Agent_Operator && count_Direct > count_Corporate
                && count_Direct > count_Electronic_Distribution) {
            channelPrefered = DistributionChannel.DIRECT;
        } else if (count_Corporate > count_Travel_Agent_Operator && count_Corporate > count_Direct
                && count_Corporate > count_Electronic_Distribution) {
            channelPrefered = DistributionChannel.CORPORATE;
        } else {
            channelPrefered = DistributionChannel.ELECTRONIC_DISTRIBUTION;
        }

        this.distributionChannelGeneral = channelPrefered;

        return this.distributionChannelGeneral;
    }

    // #endregion

    // #region VALUE RESERVATION

    /**
     * Para o indicador de score monetização, realizar-se-á a média total do valor
     * de compras realizadas. (lodgingRevenue + otherRevenue / nº total de reservas)
     */
    @Override
    public double getAverageValueReservation(ArrayList<Reservation> reservations) {
        if (reservations.isEmpty()) {
            throw new IllegalArgumentException("O array de reservas está vazio!!!");
        }

        double amount = 0;

        for (int i = 0; i < reservations.size(); i++) {
            amount += (reservations.get(i).getLodgingRevenue() + reservations.get(i).getOtherRevenue());
        }

        this.averageValueReservation = amount / (double) reservations.size();

        return this.averageValueReservation;
    }

    /**
     * Para o indicador de score monetização, verificar qual o menor valor de
     * compras realizadas. (lodgingRevenue + otherRevenue)
     */
    @Override
    public double getMinValueReservation(ArrayList<Reservation> reservations) {
        if (reservations.isEmpty()) {
            throw new IllegalArgumentException("O array de reservas está vazio!!!");
        }

        double minimumValue = 1000000;

        for (int i = 0; i < reservations.size(); i++) {
            if ((reservations.get(i).getLodgingRevenue() + reservations.get(i).getOtherRevenue()) < minimumValue) {
                minimumValue = (reservations.get(i).getLodgingRevenue() + reservations.get(i).getOtherRevenue());
            }
        }

        this.minValueReservation = minimumValue;

        return this.minValueReservation;
    }

    /**
     * Para o indicador de score monetização, verificar qual o maior valor de
     * compras realizadas. (lodgingRevenue + otherRevenue)
     */
    @Override
    public double getMaxValueReservation(ArrayList<Reservation> reservations) {
        if (reservations.isEmpty()) {
            throw new IllegalArgumentException("O array de reservas está vazio!!!");
        }

        double maximumValue = 0;

        for (int i = 0; i < reservations.size(); i++) {
            if ((reservations.get(i).getLodgingRevenue() + reservations.get(i).getOtherRevenue()) > maximumValue) {
                maximumValue = (reservations.get(i).getLodgingRevenue() + reservations.get(i).getOtherRevenue());
            }
        }

        this.maxValueReservation = maximumValue;

        return this.maxValueReservation;
    }

    // #endregion

    // #region PAYMENT METHOD

    /**
     * Qual o método de pagamento mais utilizado no geral para o pagamento das
     * reservas
     */
    @Override
    public String getPaymentMethodGeneral(ArrayList<Reservation> reservations) {
        if (reservations.isEmpty()) {
            throw new IllegalArgumentException("O array de reservas está vazio!!!");
        }

        int numberReservationsWithPaymentMethod1 = 0;
        int numberReservationsWithPaymentMethod2 = 0;
        int numberReservationsWithPaymentMethod3 = 0;
        int numberReservationsWithPaymentMethod4 = 0;

        for (int i = 0; i < reservations.size(); i++) {
            switch (reservations.get(i).getPaymentMethod()) {
                case 1:
                    numberReservationsWithPaymentMethod1++;
                    break;
                case 2:
                    numberReservationsWithPaymentMethod2++;
                    break;
                case 3:
                    numberReservationsWithPaymentMethod3++;
                    break;
                case 4:
                    numberReservationsWithPaymentMethod4++;
                    break;
                default:
                    break;
            }
        }

        int metodoPagamentoMaisUtilizado = Math.max(Math.max(numberReservationsWithPaymentMethod1, numberReservationsWithPaymentMethod2), Math.max(numberReservationsWithPaymentMethod3, numberReservationsWithPaymentMethod4));

        if (metodoPagamentoMaisUtilizado == numberReservationsWithPaymentMethod1) {
            this.paymentMethodGeneral = 1;
        } else if (metodoPagamentoMaisUtilizado == numberReservationsWithPaymentMethod2) {
            this.paymentMethodGeneral = 2;
        } else if (metodoPagamentoMaisUtilizado == numberReservationsWithPaymentMethod3) {
            this.paymentMethodGeneral = 3;
        } else {
            this.paymentMethodGeneral = 4;
        }

        return (this.paymentMethodGeneral == 1) ? "Payment Providers"
                : ((this.paymentMethodGeneral == 2) ? "Numerario"
                        : ((this.paymentMethodGeneral == 3) ? "Cartao de Credito" : "Transferencia Bancaria"));
    }

    /**
     * Qual o método de pagamento preferido por cada cliente para o pagamento da
     * reserva
     */
    @Override
    public String getPaymentMethodByClient(Client client) {
        if (client.getReservations().isEmpty()) {
            throw new IllegalArgumentException("O array de reservas está vazio!!!");
        }

        int numberReservationsWithPaymentMethod1 = 0;
        int numberReservationsWithPaymentMethod2 = 0;
        int numberReservationsWithPaymentMethod3 = 0;
        int numberReservationsWithPaymentMethod4 = 0;

        for (int i = 0; i < client.getReservations().size(); i++) {
            switch (client.getReservations().get(i).getPaymentMethod()) {
                case 1:
                    numberReservationsWithPaymentMethod1++;
                    break;
                case 2:
                    numberReservationsWithPaymentMethod2++;
                    break;
                case 3:
                    numberReservationsWithPaymentMethod3++;
                    break;
                case 4:
                    numberReservationsWithPaymentMethod4++;
                    break;
                default:
                    break;
            }
        }

        int metodoPagamentoMaisUtilizado = Math.max(
                Math.max(numberReservationsWithPaymentMethod1, numberReservationsWithPaymentMethod2),
                Math.max(numberReservationsWithPaymentMethod3, numberReservationsWithPaymentMethod4));

        if (metodoPagamentoMaisUtilizado == numberReservationsWithPaymentMethod1) {
            client.getEstatisticasCliente().setPaymentMethodPreference(1);
        } else if (metodoPagamentoMaisUtilizado == numberReservationsWithPaymentMethod2) {
            client.getEstatisticasCliente().setPaymentMethodPreference(2);
        } else if (metodoPagamentoMaisUtilizado == numberReservationsWithPaymentMethod3) {
            client.getEstatisticasCliente().setPaymentMethodPreference(3);
        } else {
            client.getEstatisticasCliente().setPaymentMethodPreference(4);
        }

        return (client.getEstatisticasCliente().getPaymentMethodPreference() == 1) ? "Payment Providers"
                : ((client.getEstatisticasCliente().getPaymentMethodPreference() == 2) ? "Numerario"
                        : ((client.getEstatisticasCliente().getPaymentMethodPreference() == 3) ? "Cartao de Credito"
                                : "Transferencia Bancaria"));
    }

    // #endregion

    // #region INTERVAL TIME BETWEEN PURCHASES

    /**
     * Qual a média de intervalo de tempo entre compras em geral, ou seja, fazer a
     * média de intervalo de compras para cada cliente, somar e dividir pelo número
     * de clientes
     */
    @Override
    public double getAverageIntervalTimeBetweenPurchasesGeneral(ArrayList<Reservation> reservations) {
        // #region VARIAVEIS

        long days = 0;
        int interval = 0;
        double avg = 0;

        // #endregion

        if (reservations.isEmpty()) {
            throw new IndexOutOfBoundsException("O array de reservas está vazio!!!");
        }

        for (int i = 0; i < reservations.size() - 1; i++) {
            // calcular o intervalo entre a atual e a reserva seguinte
            Period period = Period.between(reservations.get(i).getPurchasesDate(),
                    reservations.get(i + 1).getPurchasesDate());
            days = period.toTotalMonths() * 30 + period.getDays(); // converter o intervalo em dias

            interval += days;
        }
        // calcular a média de intervalo
        avg = interval / (double) (reservations.size());

        this.averageIntervalTimeBetweenPurchasesGeneral = avg;

        return this.averageIntervalTimeBetweenPurchasesGeneral;
    }

    /**
     * Qual a média de intervalo de tempo entre compras em cada cliente, ou seja, se
     * um cliente tiver feito 4 compras, ver o intervalo de tempo entre a 1 e 2
     * compra, entre a 2 e a 3 compra e entre a 3 e 4 compra, somar e dividir por 3
     */
    @Override
    public double getAverageIntervalTimeBetweenPurchasesByClient(Client client) {
        // #region VARIAVEIS

        long days = 0;
        int interval = 0;
        double avg = 0;

        // #endregion

        for (int i = 0; i < client.getReservations().size() - 1; i++) {
            // calcular o intervalo entre a atual e a reserva seguinte
            Period period = Period.between(client.getReservations().get(i).getPurchasesDate(),
                    client.getReservations().get(i + 1).getPurchasesDate());
            days = period.toTotalMonths() * 30 + period.getDays(); // converter o intervalo em dias

            interval += days;
        }
        // calcular a média de intervalo
        avg = interval / (double) (client.getReservations().size());

        client.getEstatisticasCliente().setMeanIntervalTimeBetweenPurchases(avg);

        return client.getEstatisticasCliente().getMeanIntervalTimeBetweenPurchases();
    }

    /**
     * Qual o intervalo de tempo máximo entre compras para cada cliente, isto é
     * verifica-se o intervalo de tempo entre a 1 e 2 compra, entre a 2 e a 3 compra
     * e entre a 3 e 4 compra, e depois compara-se qual dos intervalos de tempos é
     * maior
     */
    @Override
    public long getMaxIntervalTimeBetweenPurchasesClient(Client client) {

        long intervalMax = 0;
        ArrayList<LocalDate> datas = new ArrayList<>();

        if (client.getReservations().size() == 1) { // Caso exista apenas uma reserva
            intervalMax = 0;
        } else {

            // Criar Array apenas com as datas de cada reserva

            for (int i = 0; i < client.getReservations().size(); i++) {
                datas.add(client.getReservations().get(i).getPurchasesDate());
            }

            // Ordenar array de acordo com as datas
            Collections.sort(datas);

            // print out the minimum interval between the dates
            for (int i = 0; i < datas.size() - 1; i++) {
                if (datas.get(i + 1).toEpochDay() - datas.get(i).toEpochDay() > intervalMax) {
                    intervalMax = datas.get(i + 1).toEpochDay() - datas.get(i).toEpochDay();
                }
            }

            client.getEstatisticasCliente().setMaxIntervalTimeBetweenPurchases(intervalMax);
        }

        return client.getEstatisticasCliente().getMaxIntervalTimeBetweenPurchases();
    }

    /**
     * Qual o intervalo de tempo máximo entre compras em geral, ou seja, na
     * totalidade de clientes, isto é, verifica-se o máximo de cada cliente e depois
     * o máximo entre todos os clientes
     */
    @Override
    public long getMaxIntervalTimeBetweenPurchasesGeneral(ArrayList<Client> clients) {
        long maxIntervalTime = 0;

        if (clients.isEmpty()) {
            throw new IndexOutOfBoundsException("O array de clientes está vazio!!!");
        }
        for (int i = 0; i < clients.size(); i++) {
            clients.get(i).getEstatisticasCliente()
                    .setMaxIntervalTimeBetweenPurchases(getMaxIntervalTimeBetweenPurchasesClient(clients.get(i)));
        }

        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).getEstatisticasCliente().getMaxIntervalTimeBetweenPurchases() > maxIntervalTime) {
                maxIntervalTime = clients.get(i).getEstatisticasCliente().getMaxIntervalTimeBetweenPurchases();
            }
        }

        this.maxIntervalTimeBetweenPurchasesGeneral = maxIntervalTime;

        return this.maxIntervalTimeBetweenPurchasesGeneral;

    }

    /**
     * Qual o intervalo de tempo mínimo entre compras para cada cliente, isto é
     * verifica-se o intervalo de tempo entre a 1 e 2 compra, entre a 2 e a 3 compra
     * e entre a 3 e 4 compra, e depois compara-se qual dos intervalos de tempos é
     * menor
     */
    @Override
    public long getMinIntervalTimeBetweenPurchasesClient(Client clients) {
        long intervalMin = Long.MAX_VALUE;
        ArrayList<LocalDate> datas = new ArrayList<>();

        if (clients.getReservations().isEmpty()) {
            throw new IndexOutOfBoundsException("O array de clientes está vazio!!!");
        }

        if (clients.getReservations().size() == 1) { // Caso exista apenas uma reserva
            intervalMin = 0;
        } else {

            // Criar Array apenas com as datas de cada reserva

            for (int i = 0; i < clients.getReservations().size(); i++) {
                datas.add(clients.getReservations().get(i).getPurchasesDate());
            }

            // Ordenar array de acordo com as datas
            Collections.sort(datas);

            // print out the minimum interval between the dates
            for (int i = 0; i < datas.size() - 1; i++) {
                if (datas.get(i + 1).toEpochDay() - datas.get(i).toEpochDay() < intervalMin) {
                    intervalMin = datas.get(i + 1).toEpochDay() - datas.get(i).toEpochDay();
                }
            }

            clients.getEstatisticasCliente().setMinIntervalTimeBetweenPurchases(intervalMin);

        }

        return clients.getEstatisticasCliente().getMinIntervalTimeBetweenPurchases();
    }

    /**
     * Qual o intervalo de tempo minímo entre compras em geral, ou seja, na
     * totalidade de clientes, isto é, verifica-se o mínimo de cada cliente e depois
     * o mínimo entre todos os clientes
     */
    @Override
    public long getMinIntervalTimeBetweenPurchasesGeneral(ArrayList<Client> clients) {
        long minIntervalTime = Integer.MAX_VALUE;

        if (clients.isEmpty()) {
            throw new IndexOutOfBoundsException("O array de clientes está vazio!!!");
        }

        for (int i = 0; i < clients.size(); i++) {

            clients.get(i).getEstatisticasCliente()
                    .setMinIntervalTimeBetweenPurchases(getMinIntervalTimeBetweenPurchasesClient(clients.get(i)));
        }

        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).getEstatisticasCliente().getMinIntervalTimeBetweenPurchases() < minIntervalTime) {
                minIntervalTime = clients.get(i).getEstatisticasCliente().getMinIntervalTimeBetweenPurchases();
            }
        }

        this.minIntervalTimeBetweenPurchasesGeneral = minIntervalTime;

        return this.minIntervalTimeBetweenPurchasesGeneral;
    }

    // #endregion

    // #region ALL SCORE
    /**
     * Get customer who has the highest monetary amount spent on hotel reservations
     * 
     * @param clients {@Link ClientScoreManagement#clientes clientes}
     * @return a Client
     */
    @Override
    public Client getClientMaxValueMonetization(ArrayList<Client> clients) {
        if (clients.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        double maxValuePurchase = 0;
        Client maxValueClient = null;
        for (int i = 0; i < clients.size(); i++) {
            for (int j = 0; j < clients.get(i).getReservations().size(); j++) {
                if ((clients.get(i).getReservations().get(j).getOtherRevenue()
                        + clients.get(i).getReservations().get(j).getLodgingRevenue()) > maxValuePurchase) {
                    maxValuePurchase = clients.get(i).getReservations().get(j).getOtherRevenue()
                            + clients.get(i).getReservations().get(j).getLodgingRevenue();
                    maxValueClient = clients.get(i);
                }
            }
        }

        this.clientMaxValueMonetization = maxValueClient;

        return this.clientMaxValueMonetization;
    }

    /**
     * Get the customer with the lowest amount of money spent on hotel reservations
     * 
     * @param clients {@Link ClientScoreManagement#clientes clientes}
     * @return a Client
     */
    @Override
    public Client getClientMinValueMonetization(ArrayList<Client> clients) {
        if (clients.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        double minValuePurchase = 10000;
        Client minValueClient = null;
        for (int i = 0; i < clients.size(); i++) {
            for (int j = 0; j < clients.get(i).getReservations().size(); j++) {
                if ((clients.get(i).getReservations().get(j).getOtherRevenue()
                        + clients.get(i).getReservations().get(j).getLodgingRevenue()) < minValuePurchase) {
                    minValuePurchase = clients.get(i).getReservations().get(j).getOtherRevenue()
                            + clients.get(i).getReservations().get(j).getLodgingRevenue();
                    minValueClient = clients.get(i);
                }
            }
        }

        this.clientMinValueMonetization = minValueClient;

        return this.clientMinValueMonetization;
    }

    /**
     * Obter o cliente com a maior regularidade de compras
     * 
     * @param clients {@Link ClientScoreManagement#clientes clientes}
     * @returna a Client
     */
    @Override
    public Client getClientMaxRegularity(ArrayList<Client> clients) {
        if (clients.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        long minDaysReservation = 1054, days = 0;
        Client maxRegularityClient = null;
        ArrayList<LocalDate> datas = new ArrayList<>();
        int i;

        for (i = 0; i < clients.size(); i++) {
            if (clients.get(i).getReservations().size() <= 1) // Caso exista apenas uma reserva
            {
                days = 0;

            } else {
                days = 0;
                datas = new ArrayList<>();

                // Criar Array apenas com as datas de cada reserva
                for (int k = 0; k < clients.get(i).getReservations().size(); k++) {
                    datas.add(clients.get(i).getReservations().get(k).getPurchasesDate());
                }

                // Ordenar array de acordo com as datas
                Collections.sort(datas);

                // somando os intervalos entre reservas
                for (int j = 0; j < datas.size() - 1; j++) {
                    days += datas.get(j + 1).toEpochDay() - datas.get(j).toEpochDay();
                }

                if ((days / (clients.get(i).getReservations().size() - 1)) <= minDaysReservation
                        && (days / (clients.get(i).getReservations().size() - 1)) != 0) // se os intervalos de tempo
                                                                                        // entre
                                                                                        // reservas foi mais pequeno que
                                                                                        // as dos clientes anteriores
                {
                    minDaysReservation = (days / (clients.get(i).getReservations().size() - 1));
                    maxRegularityClient = clients.get(i);
                }
            }
        }
        this.clientMaxRegularity = maxRegularityClient;

        return this.clientMaxRegularity;
    }

    /**
     * Obter o cliente com a menor regularidade de compras
     * 
     * @param clients {@Link ClientScoreManagement#clientes clientes}
     * @return a Client
     */
    @Override
    public Client getClientMinRegularity(ArrayList<Client> clients) {
        if (clients.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        long minDaysReservation = 0, days = 0;
        Client minRegularityClient = null;
        ArrayList<LocalDate> datas = new ArrayList<>();
        int i;

        for (i = 0; i < clients.size(); i++) {
            if (clients.get(i).getReservations().size() <= 1) // Caso exista apenas uma reserva
            {
                days = 0;

            } else {
                days = 0;
                datas = new ArrayList<>();

                // Criar Array apenas com as datas de cada reserva
                for (int k = 0; k < clients.get(i).getReservations().size(); k++) {
                    datas.add(clients.get(i).getReservations().get(k).getPurchasesDate());
                }

                // Ordenar array de acordo com as datas
                Collections.sort(datas);

                // somando os intervalos entre reservas
                for (int j = 0; j < datas.size() - 1; j++) {
                    days += datas.get(j + 1).toEpochDay() - datas.get(j).toEpochDay();
                }

                if ((days / (clients.get(i).getReservations().size() - 1)) > minDaysReservation
                        && (days / (clients.get(i).getReservations().size() - 1)) != 0) // se os intervalos de tempo
                                                                                        // entre
                                                                                        // reservas foi maior que
                                                                                        // as dos clientes anteriores
                {
                    minDaysReservation = (days / (clients.get(i).getReservations().size() - 1));
                    minRegularityClient = clients.get(i);
                }
            }
        }
        this.clientMinRegularity = minRegularityClient;

        return this.clientMinRegularity;
    }

    /**
     * Obtain the customer with the maximum amount of total reservations made at the
     * hotel
     * 
     * @param clients {@Link ClientScoreManagement#clientes clientes}
     * @return a client
     */
    @Override
    public Client getClientMaxTotalBookings(ArrayList<Client> clients) {
        int size = 0;
        Client cliente = null;

        if (clients.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).getReservations().size() > size) {
                size = clients.get(i).getReservations().size();
                cliente = clients.get(i);
            }
        }

        this.clientMaxTotalBookings = cliente;

        return this.clientMaxTotalBookings;

    }

    /**
     * Get the customer with the minimum amount of total reservations made at the
     * hotel
     * 
     * @param clients {@Link ClientScoreManagement#clientes clientes}
     * @return a client
     */
    @Override
    public Client getClientMinTotalBookings(ArrayList<Client> clients) {
        int size = Integer.MAX_VALUE;
        Client cliente = null;

        if (clients.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).getReservations().size() < size) {
                size = clients.get(i).getReservations().size();
                cliente = clients.get(i);
            }
        }
        this.clientMinTotalBookings = cliente;

        return this.clientMinTotalBookings;
    }

    /**
     * Get the customer with the best average scores
     * 
     * @param clients {@Link ClientScoreManagement#clientes clientes}
     * @return a client
     */
    public Client getClientBetterAverageScore(ArrayList<Client> clients) {

        if (clients.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        Client betterAverageScoreClient = null;
        double avg = 0;
        int i;

        for (i = 0; i < clients.size(); i++) {
            // obter os scores do cliente
            if (((clients.get(i).getEstatisticasCliente().getRegularidadeScore()
                    + clients.get(i).getEstatisticasCliente().getTotalComprasScore()
                    + clients.get(i).getEstatisticasCliente().getMonetizacaoScore()) / (double) 3) > avg) // se a media for
                                                                                                 // melhor
                                                                                                 // do que o outro                                                                                     // cliente
            {
                avg = ((clients.get(i).getEstatisticasCliente().getRegularidadeScore()
                        + clients.get(i).getEstatisticasCliente().getTotalComprasScore()
                        + clients.get(i).getEstatisticasCliente().getMonetizacaoScore()) / (double) 3);

                betterAverageScoreClient = clients.get(i);
            }
        }

        this.clientBetterAverageScore = betterAverageScoreClient;

        return this.clientBetterAverageScore;

    }

    /**
     * obter o cliente com pior media de score (media mais baixa dos scores)
     * 
     * @param clients {@Link ClientScoreManagement#clientes clientes}
     * @return a client
     */
    public Client getClientWorstAverageScore(ArrayList<Client> clients) {
        if (clients.isEmpty()) {
            throw new IllegalArgumentException("Lista de clientes vazia");
        }

        Client worstAverageScoreClient = null;
        double avg = 4;
        int i;

        for (i = 0; i < clients.size(); i++) {
            // obter os scores do cliente
            if (((clients.get(i).getEstatisticasCliente().getRegularidadeScore()
                    + clients.get(i).getEstatisticasCliente().getTotalComprasScore()
                    + clients.get(i).getEstatisticasCliente().getMonetizacaoScore()) / (double) 3) < avg) // se a media for pior
                                                                                                 // do que o outro
                                                                                                 // cliente
            {
                avg = ((clients.get(i).getEstatisticasCliente().getRegularidadeScore()
                        + clients.get(i).getEstatisticasCliente().getTotalComprasScore()
                        + clients.get(i).getEstatisticasCliente().getMonetizacaoScore()) / (double) 3);

                worstAverageScoreClient = clients.get(i);
            }
        }

        this.clientWorstAverageScore = worstAverageScoreClient;

        return this.clientWorstAverageScore;
    }

    // #endregion

    public void assignEverything(ArrayList<Client> clients, ArrayList<Reservation> reservations) {
        this.getAverageIntervalTimeBetweenPurchasesGeneral(reservations);
        this.getClientMaxRegularity(clients);
        this.getClientMaxTotalBookings(clients);
        this.getClientMaxValueMonetization(clients);
        this.getClientMinValueMonetization(clients);
        this.getDistributionChannelGeneral(reservations);
        this.getPaymentMethodGeneral(reservations);
        this.getMaxIntervalTimeBetweenPurchasesGeneral(clients);
        this.getPreferedSeasonalityGeneral(reservations);
        this.getMinIntervalTimeBetweenPurchasesGeneral(clients);
        this.getMaxValueReservation(reservations);
        this.getMinValueReservation(reservations);
        this.getAverageValueReservation(reservations);
        this.getClientMinRegularity(clients);
        this.getClientMinTotalBookings(clients);
        for (int i = 0; i < clients.size(); i++) {
            this.getPaymentMethodByClient(clients.get(i));
            this.getAverageIntervalTimeBetweenPurchasesByClient(clients.get(i));
            this.getMaxIntervalTimeBetweenPurchasesClient(clients.get(i));
            this.getMinIntervalTimeBetweenPurchasesClient(clients.get(i));
            this.getPreferedSeasonalityByClient(clients.get(i));
        }
    }
}