package trabalhopratico.Classes;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class Client {
    /*
     * Transaction Identification
     */
    @Expose
    private int id;
    /*
     * Codified client's identification document
     */
    @Expose
    private String docIDHash;
    /*
     * Number of reservations canceled by the client
     */
    private int bookingsCanceled;
    /*
     * Number of reservations not checked in but also not canceled by the client
     */
    private int bookingsNoShowed;
    /**
     * Number of reservations checked in by the client
     */
    private int bookingsCheckedIn;
    /*
     * Reservation associated to the client
     */
    private ArrayList<Reservation> reservations;
    /**
     * Estatísticas do cliente
     */
    @Expose
    private StatisticsClient estatisticasCliente;

    /**
     * Método construtor
     * 
     * @param id                Transaction Identification
     * @param docIDHash         Codified client's identification document
     * @param bookingsCanceled  Number of bookings canceled by the client
     * @param bookingsNoShowed  Number of reservations not checked in but also not
     *                          canceled by the client
     * @param bookingsCheckedIn Number of reservations checked in by the client
     */
    public Client(int id, String docIDHash, int bookingsCanceled, int bookingsNoShowed, int bookingsCheckedIn) {
        this.id = id;
        this.docIDHash = docIDHash;
        this.bookingsCanceled = bookingsCanceled;
        this.bookingsNoShowed = bookingsNoShowed;
        this.bookingsCheckedIn = bookingsCheckedIn;
        this.reservations = new ArrayList<>();
        this.estatisticasCliente = new StatisticsClient();
    }

    // #region Getter Methods

    /**
     * Returns the id of the client
     * 
     * @return id of the client
     */
    public int getId() {
        return this.id;
    }

    /**
     * Returns the codified client's identification document
     * 
     * @return codified client's identification document
     */
    public String getDocIDHash() {
        return this.docIDHash;
    }

    /**
     * Returns the number of reservations canceled by the client
     * 
     * @return Number of reservations canceled by the client
     */
    public int getBookingsCanceled() {
        return this.bookingsCanceled;
    }

    /**
     * Returns the number of reservations not checked in but also not canceled by
     * the client
     * 
     * @return Number of reservations not checked in but also not canceled by the
     *         client
     */
    public int getBookingsNoShowed() {
        return this.bookingsNoShowed;
    }

    /**
     * Returns the number of reservations checked in by the client
     * 
     * @return Number of reservations checked in by the client
     */
    public int getBookingsCheckedIn() {
        return this.bookingsCheckedIn;
    }

    /**
     * Returns the reservation and it's information associated to this client
     * 
     * @return reservation associated to the client
     */
    public ArrayList<Reservation> getReservations() {
        return this.reservations;
    }

    public StatisticsClient getEstatisticasCliente() {
        return this.estatisticasCliente;
    }

    // #endregion

    // #region Setter Methods

    public void setReservations(ArrayList<Reservation> reservations) {
        this.reservations = reservations;
    }

    public void setEstatisticasCliente(StatisticsClient estatisticasCliente) {
        this.estatisticasCliente = estatisticasCliente;
    }

    // #endregion

    @Override
    public String toString() {
        return "Client [id=" + id + ", docIDHash=" + docIDHash + ", bookingsCanceled=" + bookingsCanceled
                + ", bookingsNoShowed=" + bookingsNoShowed + ", bookingsCheckedIn=" + bookingsCheckedIn
                + ", reservations=" + reservations + "]";
    }
}
